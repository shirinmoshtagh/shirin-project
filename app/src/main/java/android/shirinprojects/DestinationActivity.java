package android.shirinprojects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class DestinationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);


        String name=getIntent().getStringExtra("myname");
        Boolean isIranian=getIntent().getBooleanExtra("iranian",false);
        int age = getIntent().getIntExtra("age",18);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "ondestroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        Toast.makeText(this, "Koja ba in ajale?", Toast.LENGTH_SHORT).show();

    }
}
