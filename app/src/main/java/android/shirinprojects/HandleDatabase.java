package android.shirinprojects;

/**
 * Created by shirin on 2/6/18.
 */

public interface HandleDatabase      {
    void    connect();
    void    insertValue(String Name);
    void    deleteItem(int itemId);
}
