package android.shirinprojects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WeatherActivity extends AppCompatActivity {

    EditText cityName;
    Button showWeather;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);


        cityName = findViewById(R.id.cityName);
        showWeather = findViewById(R.id.showWeather);
        result = findViewById(R.id.result);

//        result.setText(R.string.my_name);

//        for button  with ctrl space
        showWeather.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String city = cityName.getText().toString();
                result.setText("you select " + city);
                result.setTextColor(getColor(R.color.titleColores));
                cityName.setText("");

//          you should set context for toast
                Toast.makeText(WeatherActivity.this, "salam toast", Toast.LENGTH_SHORT).show();


            }
        });


        ShirinMoshtaghClass shirinIns = new ShirinMoshtaghClass();
        shirinIns.toast(WeatherActivity.this);
        shirinIns.getName();

    }
}
