package android.shirinprojects.FoodsList;

import android.content.Context;
import android.shirinprojects.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by shirin on 2/6/18.
 */

public class FoodsListAdapter  extends BaseAdapter {

    Context mcontext ;
    List<FoodModel>  foods;

    public FoodsListAdapter(Context mcontext, List<FoodModel> foods) {
        this.mcontext = mcontext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int i) {
        return foods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View   rowview = LayoutInflater.from(mcontext).inflate(R.layout.activity_food_list,null);

        ImageView   foodImage = rowview.findViewById(R.id.foodImage);
        TextView    foodName = rowview.findViewById(R.id.foodName);
       // TextView    foodPrice   = rowview.findViewById(foodPrise);


        foodName.setText(foods.get(i).getName());
       // foodPrice.setText((foods.get(i).getPrice()+""));
        Picasso.with(mcontext).load(foods.get(i).getImage()).into(foodImage);

        return  rowview;
    }
}
