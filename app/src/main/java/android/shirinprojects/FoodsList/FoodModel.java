package android.shirinprojects.FoodsList;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by shirin on 2/6/18.
 */

public class FoodModel {

    String name;
    int price;
    String image;


    public FoodModel(String name, int price, String image) {
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public  FoodModel(){

    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
